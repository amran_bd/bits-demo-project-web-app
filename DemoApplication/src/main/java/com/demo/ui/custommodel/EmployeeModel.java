/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.ui.custommodel;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Amran Hossain 
 */
public class EmployeeModel implements Serializable{

    private Long empId;
    private String empName;
    private String designation;
    private String gender;
    private Long dateOfBirth;
    private int age;
    private byte[] profilePictuer;
    private String note;
    
    public EmployeeModel() {
    }

    public Long getEmpId() {
        return empId;
    }

    public void setEmpId(Long empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Long getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Long dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public byte[] getProfilePictuer() {
        return profilePictuer;
    }

    public void setProfilePictuer(byte[] profilePictuer) {
        this.profilePictuer = profilePictuer;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    
    
}
