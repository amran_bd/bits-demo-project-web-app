/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.ui.managedbean;

import com.demo.ui.custommodel.LoginModel;
import com.demo.ui.service.LoginService;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Amran Hossain
 */
@ManagedBean
@SessionScoped
public class LoginController {

    private LoginModel loginModel;

    @ManagedProperty(value = "#{loginService}")
    private LoginService loginService;

    /**
     * Creates a new instance of LoginController
     */
    public LoginController() {
    }

    public void checkAuthorization() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (loginModel.getUserName() != null && loginModel.getPassword() != null) {
            if (getLoginService().userLogin(loginModel.getUserName(), loginModel.getPassword())) {
                FacesContext
                        .getCurrentInstance()
                        .getApplication()
                        .getNavigationHandler()
                        .handleNavigation(FacesContext.getCurrentInstance(),
                                "null", "/dashboard.xhtml?faces-redirect=true");
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Authentication Failed!", "Credentials is not match."));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Authentication Failed!", "Given Valid Username and password"));

        }
    }

    /**
     * @return the loginModel
     */
    public LoginModel getLoginModel() {
        if (loginModel == null) {
            loginModel = new LoginModel();
        }
        return loginModel;
    }

    /**
     * @param loginModel the loginModel to set
     */
    public void setLoginModel(LoginModel loginModel) {
        this.loginModel = loginModel;
    }

    /**
     * @return the loginService
     */
    public LoginService getLoginService() {
        return loginService;
    }

    /**
     * @param loginService the loginService to set
     */
    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }

}
