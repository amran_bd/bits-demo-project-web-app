/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.ui.managedbean;

import com.demo.ui.custommodel.EmployeeModel;
import com.demo.ui.service.EmployeeService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Amran Hossain
 */
@ManagedBean
@ViewScoped
public class EmployeeController {

    private EmployeeModel employeeModel;
    private List<EmployeeModel> empList = new ArrayList<>();

    @ManagedProperty(value = "#{employeeService}")
    private EmployeeService employeeService;

    private UploadedFile file;
    private Date dateOfBirth;

    /**
     * Creates a new instance of EmployeeController
     */
    public EmployeeController() {
    }

    @PostConstruct
    public void init() {
        employeeList();
    }

    public void employeeList() {
        FacesContext context = FacesContext.getCurrentInstance();
        empList = employeeService.searchEmployee();
        if (empList.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Data Not Found!", "Data Not Found!"));
        }
    }

    public void employeeAdd() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (employeeModel != null) {
            if (getEmployeeService().addEmployee(employeeModel, dateOfBirth)) {
                employeeList();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Employee Add Success!", "Employee Add Success!"));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Employee Add Failed!", "Employee Add Failed!"));
            }
        }
    }

    public void employeeDelete(EmployeeModel employeeModel) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (getEmployeeService().deleteEmployee(employeeModel.getEmpId())) {
            employeeList();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Employee Delete Success!", "Employee Delete Success!"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Employee Delete Failed!", "Employee Delete Failed!"));
        }
    }

    public void handleFileUpload(FileUploadEvent event) {
        FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * @return the employeeModel
     */
    public EmployeeModel getEmployeeModel() {
        if (employeeModel == null) {
            employeeModel = new EmployeeModel();
        }
        return employeeModel;
    }

    /**
     * @param employeeModel the employeeModel to set
     */
    public void setEmployeeModel(EmployeeModel employeeModel) {
        this.employeeModel = employeeModel;
    }

    /**
     * @return the empList
     */
    public List<EmployeeModel> getEmpList() {
        return empList;
    }

    /**
     * @param empList the empList to set
     */
    public void setEmpList(List<EmployeeModel> empList) {
        this.empList = empList;
    }

    /**
     * @return the file
     */
    public UploadedFile getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(UploadedFile file) {
        this.file = file;
    }

    /**
     * @return the employeeService
     */
    public EmployeeService getEmployeeService() {
        return employeeService;
    }

    /**
     * @param employeeService the employeeService to set
     */
    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    /**
     * @return the dateOfBirth
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth the dateOfBirth to set
     */
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

}
