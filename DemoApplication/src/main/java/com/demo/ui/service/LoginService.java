/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.ui.service;

import com.demo.ui.util.AuthTokenInfo;
import com.demo.ui.util.Constant;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Amran Hossain
 */
@ManagedBean
@SessionScoped
public class LoginService {

    public LoginService() {
    }

    public Boolean userLogin(String userName, String password) {
        final String ACCESS_TOKEN = Constant.FCPM_NAME + AuthTokenInfo.getToken(Constant.URL, Constant.USER_NAME, Constant.PASSWORD, Constant.CLIENT_SECRET_ID, Constant.CLIENT_SECRET_KEY).getAccess_token();

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        JSONObject json = new JSONObject();
        json.put("userName", userName);
        json.put("password", password);
        HttpEntity<String> request = new HttpEntity<>(json.toString(), headers);
        RestTemplate restTemplate = new RestTemplate();
        String loginStatus = restTemplate.postForObject(Constant.URL + Constant.USER_LOGIN + ACCESS_TOKEN, request, String.class);
        if ("200".equals(loginStatus)) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

}
