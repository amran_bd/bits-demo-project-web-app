/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.ui.service;

import com.demo.ui.custommodel.EmployeeModel;
import com.demo.ui.util.AuthTokenInfo;
import com.demo.ui.util.Constant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Amran Hossain
 */
@ManagedBean
@ViewScoped
public class EmployeeService {

    public EmployeeService() {
    }

    public Boolean addEmployee(EmployeeModel employeeModel, Date dateOfBirth) {
        final String ACCESS_TOKEN = Constant.FCPM_NAME + AuthTokenInfo.getToken(Constant.URL, Constant.USER_NAME, Constant.PASSWORD, Constant.CLIENT_SECRET_ID, Constant.CLIENT_SECRET_KEY).getAccess_token();

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        JSONObject json = new JSONObject();
        json.put("empName", employeeModel.getEmpName());
        json.put("designation", employeeModel.getDesignation());
        json.put("gender", employeeModel.getGender());
        json.put("dateOfBirth", dateOfBirth.getTime());
        json.put("age", employeeModel.getAge());
        json.put("profilePictuer", employeeModel.getProfilePictuer());
        json.put("note", employeeModel.getNote());
        HttpEntity<String> request = new HttpEntity<>(json.toString(), headers);
        RestTemplate restTemplate = new RestTemplate();
        String loginStatus = restTemplate.postForObject(Constant.URL + Constant.EMP_ADD + ACCESS_TOKEN, request, String.class);
        if ("200".equals(loginStatus)) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public Boolean deleteEmployee(Long empId) {
        final String ACCESS_TOKEN = Constant.FCPM_NAME + AuthTokenInfo.getToken(Constant.URL, Constant.USER_NAME, Constant.PASSWORD, Constant.CLIENT_SECRET_ID, Constant.CLIENT_SECRET_KEY).getAccess_token();
        if (empId != null) {
            try {
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.delete(Constant.URL + Constant.EMP_DELETE + empId + ACCESS_TOKEN);
                return Boolean.TRUE;
            } catch (RestClientException e) {
                System.out.println("" + e.getMessage());
            }
        }
        return Boolean.FALSE;
    }

    public List<EmployeeModel> searchEmployee() {
        List<EmployeeModel> empList = new ArrayList<>();
        final String ACCESS_TOKEN = Constant.FCPM_NAME + AuthTokenInfo.getToken(Constant.URL, Constant.USER_NAME, Constant.PASSWORD, Constant.CLIENT_SECRET_ID, Constant.CLIENT_SECRET_KEY).getAccess_token();
        RestTemplate restTemplate = new RestTemplate();
        try {
            String listValue = restTemplate.getForObject(Constant.URL + Constant.EMP_LIST + ACCESS_TOKEN, String.class);
            if (listValue != null) {
                JSONArray jSONArray = new JSONArray(listValue);
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject indexObj = new JSONObject(jSONArray.get(i).toString());
                    EmployeeModel model = new EmployeeModel();
                    model.setEmpId(indexObj.getLong("empId"));
                    model.setEmpName(indexObj.getString("empName"));
                    model.setDesignation(indexObj.getString("designation"));
                    model.setGender(indexObj.getString("gender"));
                    model.setAge(indexObj.getInt("age"));
                    //model.setDateOfBirth(new Date(indexObj.getLong("dateOfBirth")));
                    model.setNote(indexObj.getString("note"));
                    empList.add(model);
                }
            }
        } catch (RestClientException e) {
            System.out.println("Exception" + e.getMessage());
        }
        return empList;
    }

}
