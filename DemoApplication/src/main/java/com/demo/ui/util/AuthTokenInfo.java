/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.ui.util;

import com.demo.ui.custommodel.AuthTokenModel;
import java.util.Arrays;
import org.apache.commons.codec.binary.Base64;
import java.util.LinkedHashMap;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Amran Hossain
 */
public class AuthTokenInfo {

    public static String AUTH_SERVER_URI = "/oauth/token";

    
    public static AuthTokenModel getToken(String url, String username, String password, String basicAuthAccessKey, String basicAuthScretKey) {
        RestTemplate restTemplate = new RestTemplate();
        String ENDPOINT_AUTH_SERVER_URI = url + AUTH_SERVER_URI;
        String BASE_URL = "?grant_type=password&username=" + username + "&password=" + password;
        HttpEntity<String> request = new HttpEntity<>(getHeadersWithClientCredentials(basicAuthAccessKey, basicAuthScretKey));
        ResponseEntity<Object> response = restTemplate.exchange(ENDPOINT_AUTH_SERVER_URI + BASE_URL,HttpMethod.POST, request, Object.class);
        LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>) response.getBody();
        AuthTokenModel authTokenModel = null;
        if (map != null) {
            authTokenModel = new AuthTokenModel();
            authTokenModel.setAccess_token((String) map.get("access_token"));
            authTokenModel.setToken_type((String) map.get("token_type"));
            authTokenModel.setRefresh_token((String) map.get("refresh_token"));
            authTokenModel.setExpires_in((int) map.get("expires_in"));
            authTokenModel.setScope((String) map.get("scope"));
        } else {
            System.out.println("No user exist----------");
        }
        return authTokenModel;

    }

    private static HttpHeaders getHeadersWithClientCredentials(String basickAuthAccessKey, String basicAuthSecretKey) {

        String plainClientCredentials = basickAuthAccessKey + ":" + basicAuthSecretKey;
        String base64ClientCredentials = new String(Base64.encodeBase64(plainClientCredentials.getBytes()));
        HttpHeaders headers = getHeaders();
        headers.add("Authorization", "Basic " + base64ClientCredentials);
        return headers;
    }

    private static HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        return headers;
    }

}
