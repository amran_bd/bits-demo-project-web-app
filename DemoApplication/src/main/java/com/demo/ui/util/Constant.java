/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.ui.util;

/**
 *
 * @author Amran Hossain || Sr. Developer || HLC Technologies Ltd.
 */
public class Constant {

    public static final String CLIENT_SECRET_ID = "secure-client";
    public static final String CLIENT_SECRET_KEY = "password@123";
    public static final String USER_NAME = "amran";
    public static final String PASSWORD = "amran";
    public static final String FCPM_NAME = "?access_token=";
    public static final String URL = "http://localhost:8080";
    public static final String USER_LOGIN = "/api/v1/user/login";
    public static final String EMP_LIST = "/api/v1/emp/list";
    public static final String EMP_ADD = "/api/v1/emp/add";
    public static final String EMP_DELETE = "/api/v1/emp/delete/";

}
